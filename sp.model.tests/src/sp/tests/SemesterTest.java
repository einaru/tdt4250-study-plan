/**
 */
package sp.tests;

import junit.framework.TestCase;

import sp.Semester;
import sp.SpFactory;
import sp.Term;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link sp.Semester#getName() <em>Name</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class SemesterTest extends TestCase {

	/**
	 * The fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Semester fixture = null;

	/**
	 * Constructs a new Semester test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemesterTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Semester fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Semester getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link sp.Semester#getName() <em>Name</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sp.Semester#getName()
	 * @generated NOT
	 */
	public void testGetName() {
		Semester semester = SpFactory.eINSTANCE.createProgrammeSemester();

		assertEquals(Term.AUTUMN + " 0", semester.getName());

		semester.setTerm(Term.SPRING);
		assertEquals(Term.SPRING.getLiteral() + " 0", semester.getName());

		int year = 2021;
		semester.setYear(year);
		assertEquals(Term.SPRING.getLiteral() + " " + year, semester.getName());

		semester.setTerm(Term.AUTUMN);
		assertEquals(Term.AUTUMN.getLiteral() + " " + year, semester.getName());
	}

} //SemesterTest
