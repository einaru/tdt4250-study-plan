/**
 */
package sp.tests;

import junit.textui.TestRunner;

import sp.ProgrammeSemester;
import sp.SpFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Programme Semester</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ProgrammeSemesterTest extends SemesterTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ProgrammeSemesterTest.class);
	}

	/**
	 * Constructs a new Programme Semester test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProgrammeSemesterTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Programme Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ProgrammeSemester getFixture() {
		return (ProgrammeSemester)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SpFactory.eINSTANCE.createProgrammeSemester());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ProgrammeSemesterTest
