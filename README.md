# Study Plan

This repo contains my work for the individual modelling assignments in the
course [TDT4250][] Advanced Software Design at [NTNU][], during the fall of
2020.

[NTNU]: https://www.ntnu.edu/
[TDT4250]: https://www.ntnu.edu/studies/courses/TDT4250/2020/

## Repo structure

-   [sp.model](sp.model/) — Ecore model project
-   [sp.model.tests](sp.model.tests/) — Test code for the Ecore model project
-   [sp.html](sp.html/) — M2T transformation project
