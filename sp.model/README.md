# Study Plan - Ecore Model

## Project structure

-   The [model](model/) folder contains the [EMF][] Ecore and Genmodel.
-   The [data](data/) folder contains sample instance data for the model.
-   The [src](src/) folder contains Java packages with generated Java code:
    -   [sp](src/sp) contains generated interfaces.
    -   [sp.impl](src/sp/impl) contains implementations of the interfaces.
    -   [sp.example](src/sp/example) contains example code making use of the
        generated code.

[EMF]: https://www.eclipse.org/modeling/emf/
