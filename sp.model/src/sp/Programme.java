/**
 */
package sp;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Programme</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link sp.Programme#getCode <em>Code</em>}</li>
 *   <li>{@link sp.Programme#getName <em>Name</em>}</li>
 *   <li>{@link sp.Programme#getLevel <em>Level</em>}</li>
 *   <li>{@link sp.Programme#getDuration <em>Duration</em>}</li>
 *   <li>{@link sp.Programme#getStudyStart <em>Study Start</em>}</li>
 *   <li>{@link sp.Programme#getCredit <em>Credit</em>}</li>
 *   <li>{@link sp.Programme#getDepartment <em>Department</em>}</li>
 *   <li>{@link sp.Programme#getSpecialisations <em>Specialisations</em>}</li>
 *   <li>{@link sp.Programme#getSemesters <em>Semesters</em>}</li>
 * </ul>
 *
 * @see sp.SpPackage#getProgramme()
 * @model
 * @generated
 */
public interface Programme extends EObject {
	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see sp.SpPackage#getProgramme_Code()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link sp.Programme#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see sp.SpPackage#getProgramme_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link sp.Programme#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Level</b></em>' attribute.
	 * The literals are from the enumeration {@link sp.ProgrammeLevel}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level</em>' attribute.
	 * @see sp.ProgrammeLevel
	 * @see #setLevel(ProgrammeLevel)
	 * @see sp.SpPackage#getProgramme_Level()
	 * @model required="true"
	 * @generated
	 */
	ProgrammeLevel getLevel();

	/**
	 * Sets the value of the '{@link sp.Programme#getLevel <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level</em>' attribute.
	 * @see sp.ProgrammeLevel
	 * @see #getLevel()
	 * @generated
	 */
	void setLevel(ProgrammeLevel value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(int)
	 * @see sp.SpPackage#getProgramme_Duration()
	 * @model dataType="sp.PositiveInt"
	 * @generated
	 */
	int getDuration();

	/**
	 * Sets the value of the '{@link sp.Programme#getDuration <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(int value);

	/**
	 * Returns the value of the '<em><b>Study Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study Start</em>' attribute.
	 * @see #setStudyStart(int)
	 * @see sp.SpPackage#getProgramme_StudyStart()
	 * @model dataType="sp.StudyStart" required="true"
	 * @generated
	 */
	int getStudyStart();

	/**
	 * Sets the value of the '{@link sp.Programme#getStudyStart <em>Study Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Study Start</em>' attribute.
	 * @see #getStudyStart()
	 * @generated
	 */
	void setStudyStart(int value);

	/**
	 * Returns the value of the '<em><b>Credit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credit</em>' attribute.
	 * @see #setCredit(float)
	 * @see sp.SpPackage#getProgramme_Credit()
	 * @model dataType="sp.Credit"
	 * @generated
	 */
	float getCredit();

	/**
	 * Sets the value of the '{@link sp.Programme#getCredit <em>Credit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credit</em>' attribute.
	 * @see #getCredit()
	 * @generated
	 */
	void setCredit(float value);

	/**
	 * Returns the value of the '<em><b>Department</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link sp.Department#getProgrammes <em>Programmes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Department</em>' container reference.
	 * @see #setDepartment(Department)
	 * @see sp.SpPackage#getProgramme_Department()
	 * @see sp.Department#getProgrammes
	 * @model opposite="programmes" required="true" transient="false"
	 * @generated
	 */
	Department getDepartment();

	/**
	 * Sets the value of the '{@link sp.Programme#getDepartment <em>Department</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Department</em>' container reference.
	 * @see #getDepartment()
	 * @generated
	 */
	void setDepartment(Department value);

	/**
	 * Returns the value of the '<em><b>Specialisations</b></em>' containment reference list.
	 * The list contents are of type {@link sp.Specialisation}.
	 * It is bidirectional and its opposite is '{@link sp.Specialisation#getProgramme <em>Programme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialisations</em>' containment reference list.
	 * @see sp.SpPackage#getProgramme_Specialisations()
	 * @see sp.Specialisation#getProgramme
	 * @model opposite="programme" containment="true"
	 * @generated
	 */
	EList<Specialisation> getSpecialisations();

	/**
	 * Returns the value of the '<em><b>Semesters</b></em>' containment reference list.
	 * The list contents are of type {@link sp.ProgrammeSemester}.
	 * It is bidirectional and its opposite is '{@link sp.ProgrammeSemester#getProgramme <em>Programme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semesters</em>' containment reference list.
	 * @see sp.SpPackage#getProgramme_Semesters()
	 * @see sp.ProgrammeSemester#getProgramme
	 * @model opposite="programme" containment="true"
	 * @generated
	 */
	EList<ProgrammeSemester> getSemesters();

} // Programme
