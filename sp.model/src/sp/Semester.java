/**
 */
package sp;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link sp.Semester#getYear <em>Year</em>}</li>
 *   <li>{@link sp.Semester#getTerm <em>Term</em>}</li>
 *   <li>{@link sp.Semester#getName <em>Name</em>}</li>
 *   <li>{@link sp.Semester#getMinCredit <em>Min Credit</em>}</li>
 * </ul>
 *
 * @see sp.SpPackage#getSemester()
 * @model abstract="true"
 * @generated
 */
public interface Semester extends EObject {
	/**
	 * Returns the value of the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Year</em>' attribute.
	 * @see #setYear(int)
	 * @see sp.SpPackage#getSemester_Year()
	 * @model required="true"
	 * @generated
	 */
	int getYear();

	/**
	 * Sets the value of the '{@link sp.Semester#getYear <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Year</em>' attribute.
	 * @see #getYear()
	 * @generated
	 */
	void setYear(int value);

	/**
	 * Returns the value of the '<em><b>Term</b></em>' attribute.
	 * The literals are from the enumeration {@link sp.Term}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Term</em>' attribute.
	 * @see sp.Term
	 * @see #setTerm(Term)
	 * @see sp.SpPackage#getSemester_Term()
	 * @model required="true"
	 * @generated
	 */
	Term getTerm();

	/**
	 * Sets the value of the '{@link sp.Semester#getTerm <em>Term</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Term</em>' attribute.
	 * @see sp.Term
	 * @see #getTerm()
	 * @generated
	 */
	void setTerm(Term value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see sp.SpPackage#getSemester_Name()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getName();

	/**
	 * Returns the value of the '<em><b>Min Credit</b></em>' attribute.
	 * The default value is <code>"30.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Credit</em>' attribute.
	 * @see sp.SpPackage#getSemester_MinCredit()
	 * @model default="30.0" changeable="false"
	 * @generated
	 */
	float getMinCredit();

} // Semester
