/**
 */
package sp.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import sp.Course;
import sp.CourseGroup;
import sp.Programme;
import sp.ProgrammeSemester;
import sp.SpPackage;
import sp.Specialisation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Programme Semester</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link sp.impl.ProgrammeSemesterImpl#getProgramme <em>Programme</em>}</li>
 *   <li>{@link sp.impl.ProgrammeSemesterImpl#getSpecialisation <em>Specialisation</em>}</li>
 *   <li>{@link sp.impl.ProgrammeSemesterImpl#getCompulsoryCourses <em>Compulsory Courses</em>}</li>
 *   <li>{@link sp.impl.ProgrammeSemesterImpl#getElectives <em>Electives</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProgrammeSemesterImpl extends SemesterImpl implements ProgrammeSemester {
	/**
	 * The cached value of the '{@link #getCompulsoryCourses() <em>Compulsory Courses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompulsoryCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> compulsoryCourses;

	/**
	 * The cached value of the '{@link #getElectives() <em>Electives</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElectives()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseGroup> electives;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProgrammeSemesterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpPackage.Literals.PROGRAMME_SEMESTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Programme getProgramme() {
		if (eContainerFeatureID() != SpPackage.PROGRAMME_SEMESTER__PROGRAMME) return null;
		return (Programme)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProgramme(Programme newProgramme, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newProgramme, SpPackage.PROGRAMME_SEMESTER__PROGRAMME, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProgramme(Programme newProgramme) {
		if (newProgramme != eInternalContainer() || (eContainerFeatureID() != SpPackage.PROGRAMME_SEMESTER__PROGRAMME && newProgramme != null)) {
			if (EcoreUtil.isAncestor(this, newProgramme))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newProgramme != null)
				msgs = ((InternalEObject)newProgramme).eInverseAdd(this, SpPackage.PROGRAMME__SEMESTERS, Programme.class, msgs);
			msgs = basicSetProgramme(newProgramme, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpPackage.PROGRAMME_SEMESTER__PROGRAMME, newProgramme, newProgramme));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Specialisation getSpecialisation() {
		if (eContainerFeatureID() != SpPackage.PROGRAMME_SEMESTER__SPECIALISATION) return null;
		return (Specialisation)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSpecialisation(Specialisation newSpecialisation, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newSpecialisation, SpPackage.PROGRAMME_SEMESTER__SPECIALISATION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecialisation(Specialisation newSpecialisation) {
		if (newSpecialisation != eInternalContainer() || (eContainerFeatureID() != SpPackage.PROGRAMME_SEMESTER__SPECIALISATION && newSpecialisation != null)) {
			if (EcoreUtil.isAncestor(this, newSpecialisation))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSpecialisation != null)
				msgs = ((InternalEObject)newSpecialisation).eInverseAdd(this, SpPackage.SPECIALISATION__SEMESTERS, Specialisation.class, msgs);
			msgs = basicSetSpecialisation(newSpecialisation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpPackage.PROGRAMME_SEMESTER__SPECIALISATION, newSpecialisation, newSpecialisation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getCompulsoryCourses() {
		if (compulsoryCourses == null) {
			compulsoryCourses = new EObjectResolvingEList<Course>(Course.class, this, SpPackage.PROGRAMME_SEMESTER__COMPULSORY_COURSES);
		}
		return compulsoryCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseGroup> getElectives() {
		if (electives == null) {
			electives = new EObjectContainmentEList<CourseGroup>(CourseGroup.class, this, SpPackage.PROGRAMME_SEMESTER__ELECTIVES);
		}
		return electives;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SpPackage.PROGRAMME_SEMESTER__PROGRAMME:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetProgramme((Programme)otherEnd, msgs);
			case SpPackage.PROGRAMME_SEMESTER__SPECIALISATION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetSpecialisation((Specialisation)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SpPackage.PROGRAMME_SEMESTER__PROGRAMME:
				return basicSetProgramme(null, msgs);
			case SpPackage.PROGRAMME_SEMESTER__SPECIALISATION:
				return basicSetSpecialisation(null, msgs);
			case SpPackage.PROGRAMME_SEMESTER__ELECTIVES:
				return ((InternalEList<?>)getElectives()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case SpPackage.PROGRAMME_SEMESTER__PROGRAMME:
				return eInternalContainer().eInverseRemove(this, SpPackage.PROGRAMME__SEMESTERS, Programme.class, msgs);
			case SpPackage.PROGRAMME_SEMESTER__SPECIALISATION:
				return eInternalContainer().eInverseRemove(this, SpPackage.SPECIALISATION__SEMESTERS, Specialisation.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpPackage.PROGRAMME_SEMESTER__PROGRAMME:
				return getProgramme();
			case SpPackage.PROGRAMME_SEMESTER__SPECIALISATION:
				return getSpecialisation();
			case SpPackage.PROGRAMME_SEMESTER__COMPULSORY_COURSES:
				return getCompulsoryCourses();
			case SpPackage.PROGRAMME_SEMESTER__ELECTIVES:
				return getElectives();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpPackage.PROGRAMME_SEMESTER__PROGRAMME:
				setProgramme((Programme)newValue);
				return;
			case SpPackage.PROGRAMME_SEMESTER__SPECIALISATION:
				setSpecialisation((Specialisation)newValue);
				return;
			case SpPackage.PROGRAMME_SEMESTER__COMPULSORY_COURSES:
				getCompulsoryCourses().clear();
				getCompulsoryCourses().addAll((Collection<? extends Course>)newValue);
				return;
			case SpPackage.PROGRAMME_SEMESTER__ELECTIVES:
				getElectives().clear();
				getElectives().addAll((Collection<? extends CourseGroup>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpPackage.PROGRAMME_SEMESTER__PROGRAMME:
				setProgramme((Programme)null);
				return;
			case SpPackage.PROGRAMME_SEMESTER__SPECIALISATION:
				setSpecialisation((Specialisation)null);
				return;
			case SpPackage.PROGRAMME_SEMESTER__COMPULSORY_COURSES:
				getCompulsoryCourses().clear();
				return;
			case SpPackage.PROGRAMME_SEMESTER__ELECTIVES:
				getElectives().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpPackage.PROGRAMME_SEMESTER__PROGRAMME:
				return getProgramme() != null;
			case SpPackage.PROGRAMME_SEMESTER__SPECIALISATION:
				return getSpecialisation() != null;
			case SpPackage.PROGRAMME_SEMESTER__COMPULSORY_COURSES:
				return compulsoryCourses != null && !compulsoryCourses.isEmpty();
			case SpPackage.PROGRAMME_SEMESTER__ELECTIVES:
				return electives != null && !electives.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ProgrammeSemesterImpl
