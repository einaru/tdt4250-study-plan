package sp.example;

import java.util.Iterator;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import sp.Course;
import sp.Department;
import sp.SpPackage;
import sp.University;

public class ResourceLoader {

	public static void main(String[] args) {
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getPackageRegistry().put(SpPackage.eINSTANCE.getNsURI(), SpPackage.eINSTANCE);
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());

		Resource resource = resourceSet.getResource(URI.createURI("data/idi-courses.xmi"), true);

		University uni = (University) resource.getContents().get(0);
		for (Department dep : uni.getDepartments()) {
			System.out.println(dep.getName());

			int n = 0;
			Iterator<Course> courses = dep.getCourses().iterator();
			while (courses.hasNext()) {
				Course c = courses.next();
				String p = courses.hasNext() ? "├──" : "└──";
				System.out.println(String.format("%s %s %s", p, c.getCode(), c.getName()));
				n++;
			}
			System.out.println(String.format("%d %s", n, n == 1 ? "course" : "courses"));
			System.out.println();
		}
	}
}
