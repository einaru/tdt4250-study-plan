/**
 */
package sp;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Programme Semester</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link sp.ProgrammeSemester#getProgramme <em>Programme</em>}</li>
 *   <li>{@link sp.ProgrammeSemester#getSpecialisation <em>Specialisation</em>}</li>
 *   <li>{@link sp.ProgrammeSemester#getCompulsoryCourses <em>Compulsory Courses</em>}</li>
 *   <li>{@link sp.ProgrammeSemester#getElectives <em>Electives</em>}</li>
 * </ul>
 *
 * @see sp.SpPackage#getProgrammeSemester()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='yearIsValid'"
 * @generated
 */
public interface ProgrammeSemester extends Semester {
	/**
	 * Returns the value of the '<em><b>Programme</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link sp.Programme#getSemesters <em>Semesters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Programme</em>' container reference.
	 * @see #setProgramme(Programme)
	 * @see sp.SpPackage#getProgrammeSemester_Programme()
	 * @see sp.Programme#getSemesters
	 * @model opposite="semesters" transient="false"
	 * @generated
	 */
	Programme getProgramme();

	/**
	 * Sets the value of the '{@link sp.ProgrammeSemester#getProgramme <em>Programme</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Programme</em>' container reference.
	 * @see #getProgramme()
	 * @generated
	 */
	void setProgramme(Programme value);

	/**
	 * Returns the value of the '<em><b>Specialisation</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link sp.Specialisation#getSemesters <em>Semesters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialisation</em>' container reference.
	 * @see #setSpecialisation(Specialisation)
	 * @see sp.SpPackage#getProgrammeSemester_Specialisation()
	 * @see sp.Specialisation#getSemesters
	 * @model opposite="semesters" transient="false"
	 * @generated
	 */
	Specialisation getSpecialisation();

	/**
	 * Sets the value of the '{@link sp.ProgrammeSemester#getSpecialisation <em>Specialisation</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specialisation</em>' container reference.
	 * @see #getSpecialisation()
	 * @generated
	 */
	void setSpecialisation(Specialisation value);

	/**
	 * Returns the value of the '<em><b>Compulsory Courses</b></em>' reference list.
	 * The list contents are of type {@link sp.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compulsory Courses</em>' reference list.
	 * @see sp.SpPackage#getProgrammeSemester_CompulsoryCourses()
	 * @model
	 * @generated
	 */
	EList<Course> getCompulsoryCourses();

	/**
	 * Returns the value of the '<em><b>Electives</b></em>' containment reference list.
	 * The list contents are of type {@link sp.CourseGroup}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Electives</em>' containment reference list.
	 * @see sp.SpPackage#getProgrammeSemester_Electives()
	 * @model containment="true"
	 * @generated
	 */
	EList<CourseGroup> getElectives();

} // ProgrammeSemester
