/**
 */
package sp;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Study Level</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see sp.SpPackage#getStudyLevel()
 * @model
 * @generated
 */
public enum StudyLevel implements Enumerator {
	/**
	 * The '<em><b>EXAMEN PHILOSOPHICUM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EXAMEN_PHILOSOPHICUM_VALUE
	 * @generated
	 * @ordered
	 */
	EXAMEN_PHILOSOPHICUM(0, "EXAMEN_PHILOSOPHICUM", "Examen Philosophicum"),

	/**
	 * The '<em><b>FOUNDATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FOUNDATION_VALUE
	 * @generated
	 * @ordered
	 */
	FOUNDATION(1, "FOUNDATION", "Foundation"),

	/**
	 * The '<em><b>INTERMEDIATE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTERMEDIATE_VALUE
	 * @generated
	 * @ordered
	 */
	INTERMEDIATE(2, "INTERMEDIATE", "Intermediate"),

	/**
	 * The '<em><b>THIRD YEAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #THIRD_YEAR_VALUE
	 * @generated
	 * @ordered
	 */
	THIRD_YEAR(3, "THIRD_YEAR", "Third Year"),

	/**
	 * The '<em><b>SECOND DEGREE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SECOND_DEGREE_VALUE
	 * @generated
	 * @ordered
	 */
	SECOND_DEGREE(4, "SECOND_DEGREE", "Second Degree"),

	/**
	 * The '<em><b>DOCTORAL DEGREE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DOCTORAL_DEGREE_VALUE
	 * @generated
	 * @ordered
	 */
	DOCTORAL_DEGREE(5, "DOCTORAL_DEGREE", "Doctoral Degree");

	/**
	 * The '<em><b>EXAMEN PHILOSOPHICUM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EXAMEN_PHILOSOPHICUM
	 * @model literal="Examen Philosophicum"
	 * @generated
	 * @ordered
	 */
	public static final int EXAMEN_PHILOSOPHICUM_VALUE = 0;

	/**
	 * The '<em><b>FOUNDATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FOUNDATION
	 * @model literal="Foundation"
	 * @generated
	 * @ordered
	 */
	public static final int FOUNDATION_VALUE = 1;

	/**
	 * The '<em><b>INTERMEDIATE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTERMEDIATE
	 * @model literal="Intermediate"
	 * @generated
	 * @ordered
	 */
	public static final int INTERMEDIATE_VALUE = 2;

	/**
	 * The '<em><b>THIRD YEAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #THIRD_YEAR
	 * @model literal="Third Year"
	 * @generated
	 * @ordered
	 */
	public static final int THIRD_YEAR_VALUE = 3;

	/**
	 * The '<em><b>SECOND DEGREE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SECOND_DEGREE
	 * @model literal="Second Degree"
	 * @generated
	 * @ordered
	 */
	public static final int SECOND_DEGREE_VALUE = 4;

	/**
	 * The '<em><b>DOCTORAL DEGREE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DOCTORAL_DEGREE
	 * @model literal="Doctoral Degree"
	 * @generated
	 * @ordered
	 */
	public static final int DOCTORAL_DEGREE_VALUE = 5;

	/**
	 * An array of all the '<em><b>Study Level</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final StudyLevel[] VALUES_ARRAY =
		new StudyLevel[] {
			EXAMEN_PHILOSOPHICUM,
			FOUNDATION,
			INTERMEDIATE,
			THIRD_YEAR,
			SECOND_DEGREE,
			DOCTORAL_DEGREE,
		};

	/**
	 * A public read-only list of all the '<em><b>Study Level</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<StudyLevel> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Study Level</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static StudyLevel get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			StudyLevel result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Study Level</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static StudyLevel getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			StudyLevel result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Study Level</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static StudyLevel get(int value) {
		switch (value) {
			case EXAMEN_PHILOSOPHICUM_VALUE: return EXAMEN_PHILOSOPHICUM;
			case FOUNDATION_VALUE: return FOUNDATION;
			case INTERMEDIATE_VALUE: return INTERMEDIATE;
			case THIRD_YEAR_VALUE: return THIRD_YEAR;
			case SECOND_DEGREE_VALUE: return SECOND_DEGREE;
			case DOCTORAL_DEGREE_VALUE: return DOCTORAL_DEGREE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private StudyLevel(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //StudyLevel
