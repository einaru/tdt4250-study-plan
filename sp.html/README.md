# Study Plan - M2T Transformation

## Project structure

-   The [src](src/) folder contains Java packages with implementation details
    related to the Acceleo M2T transformation:
    -   [generate.mtl](src/sp/html/main/generate.mtl) — Acceleo module
        containing templates and queries used to transform instance data of the
        Ecore model to HTML.
    -   [Generator.java](src/sp/html/main/Generate.java) — Java class
        containing the entry point of the generation module.
    -   [Helper.java](src/sp/html/main/Helper.java) — Java class implementing
        various helper methods used in the generation module.

## Running the transformation

In order to run the M2T transformation you need [Eclipse][] with [Acceleo][]
installed.

1.  Start Eclipse and import both the [sp.html](../sp.html) and
    [sp.model](../sp.model) projects into your workspace.
2.  Right-click the **sp.html** project and select **Run As** and then
    **Run Configuration…**
3.  In the **Run Configuration** dialog right-click on **Acceleo Application**
    and select **New Configuration**
4.  Enter the following configuration values:
    -   Project: `sp.html`
    -   Main class: `sp.html.main.Generate`
    -   Model: `/sp.model/data/msit.xmi`
    -   Target: `/sp.html/src-gen`
5.  Click **Run** to run the transformation

The generated HTML file should now be located under the target folder you added
in the **Run Configuration**.

A sample HTML file, generated from the [msit.xmi](../sp.model/data/msit.xmi)
instance data file, can be seen here: [ntnu.html](sample-gen/ntnu.html).

[Eclipse]: https://www.eclipse.org/
[Acceleo]: https://www.eclipse.org/acceleo/
