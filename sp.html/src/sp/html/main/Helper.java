package sp.html.main;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import sp.Department;
import sp.Programme;
import sp.University;

public class Helper {

	public EList<Programme> getAllProgrammes(EObject object) {
		University uni = (University) object;
		EList<Programme> programmes = new BasicEList<>();
		for (Department dep : uni.getDepartments()) {
			programmes.addAll(dep.getProgrammes());
		}
		return programmes;
	}
}
